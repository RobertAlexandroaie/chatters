/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.dc.masma.model;

import ac.dc.masma.model.behaviours.AnswerTimeBehaviour;
import ac.dc.masma.model.behaviours.AskTimeBehaviour;
import ac.dc.masma.model.behaviours.ReceiveTimeAnswerBehaviour;
import jade.core.Agent;
import java.util.Random;

/**
 *
 * @author lab
 */
public class Chatter extends Agent {

    @Override
    protected void setup() {
        
        addBehaviour(new AskTimeBehaviour(this,new Random().nextInt(5)*100));
        addBehaviour(new AnswerTimeBehaviour());
        addBehaviour(new ReceiveTimeAnswerBehaviour());
    }
    
    
    
}
