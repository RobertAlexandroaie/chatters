/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.dc.masma.model.behaviours;

import ac.dc.masma.model.Chatter;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author lab
 */
public class AnswerTimeBehaviour extends CyclicBehaviour {

    private Chatter myAgent;

    @Override
    public void action() {

        ACLMessage message = myAgent.receive();

        if (message != null && message.getPerformative() == ACLMessage.REQUEST) {

            final AID sender = message.getSender();

            ACLMessage answer = new ACLMessage(ACLMessage.INFORM);
            answer.addReceiver(sender);
            
            Date date = new Date();   // given date
            Calendar calendar = GregorianCalendar.getInstance();
            calendar.setTime(date);
            final int hour = calendar.get(Calendar.HOUR);
            final int minute = calendar.get(Calendar.MINUTE);
            
            answer.setContent("[" + myAgent.getName() + " is answering] Time is "+hour+":"+minute);
            myAgent.send(answer);
        } else {
            block();
        }
    }

}
