/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.dc.masma.model.behaviours;

import ac.dc.masma.model.Chatter;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import java.util.Random;

/**
 *
 * @author lab
 */
public class AskTimeBehaviour extends TickerBehaviour{

    private Chatter myAgent;

    public AskTimeBehaviour(Agent a, long period) {
        super(a, period);
    }
  
    @Override
    protected void onTick() {
        
        ACLMessage message = new ACLMessage(ACLMessage.REQUEST);
        
        final String chatter1 = "Chatter"+new Random().nextInt(5);
        AID receiverAID = new AID(chatter1, AID.ISLOCALNAME); // send to an agent

        message.addReceiver(receiverAID);
        message.setContent("["+myAgent.getName()+" is asking] What time is it?");
        
        myAgent.send(message);
    }
    
    
}
