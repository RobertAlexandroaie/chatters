/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.dc.masma.model.behaviours;

import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;

/**
 *
 * @author lab
 */
public class ReceiveTimeAnswerBehaviour extends CyclicBehaviour {

    @Override
    public void action() {

        ACLMessage message = myAgent.receive();
        
        if (message != null && message.getPerformative() == ACLMessage.INFORM) {
            
            System.out.println(message.getContent());

        } else {
            
            block();
        }
    }

}
