/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.dc.masma.main;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.core.Runtime;

/**
 *
 * @author lab
 */
public class Chatters {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
         try {
          Runtime rt = Runtime.instance();
          Profile pMain = new ProfileImpl();
          AgentContainer mc = rt.createMainContainer(pMain);

          AgentController ag1 = mc.createNewAgent("Agent1", "lab2example1.Agent1", args);
          ag1.start();
        }
        catch(Exception e) {
        }
    }
    
}
